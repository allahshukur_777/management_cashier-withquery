package com.example.managementCashier.exception;

public class ConnectException extends RuntimeException {

    public ConnectException(String message) {
        super(message);
    }

}
