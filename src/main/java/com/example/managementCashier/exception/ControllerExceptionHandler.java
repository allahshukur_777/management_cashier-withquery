package com.example.managementCashier.exception;

import java.time.DateTimeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

//    @ExceptionHandler(value = {ConnectException.class})
//    public ResponseEntity<ErrorMessage> connectException(ConnectException ex) {
//        Integer httpStatus = HttpStatus.CONTINUE.value();
//        ErrorMessage message = new ErrorMessage(
//                httpStatus,
//                ex.getMessage());
//        return new ResponseEntity<>(message, HttpStatus.CONTINUE);
//    }

//    @ExceptionHandler(value = {StringException.class})
//    public ResponseEntity<ErrorMessage> stringException(StringException ex) {
//        Integer httpStatus = HttpStatus.OK.value();
//        ErrorMessage message = new ErrorMessage(
//                httpStatus,
//                ex.getMessage());
//        return new ResponseEntity<>(message, HttpStatus.OK);
//    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> notFoundException(NotFoundException n) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                n.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DateTimeException.class)
    public ResponseEntity<Object> dateTimeException(DateTimeException dt) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value(); ;
        ErrorMessage message = new ErrorMessage(
                httpStatus,
                dt.getMessage());
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

}
