package com.example.managementCashier.exception;

import lombok.Data;

@Data
public class StringException extends RuntimeException{
//    private final Integer statusCode;

    public StringException(String message){
        super(message);
    }

//    public OkException(String message, Integer statusCode){
//        super(message);
//        this.statusCode= statusCode;
//    }

}
