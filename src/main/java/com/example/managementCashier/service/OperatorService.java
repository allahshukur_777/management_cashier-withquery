package com.example.managementCashier.service;

import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import java.util.List;

public interface OperatorService<T>{

    List<T> getAll(Integer page, Integer size);

    OperatorSaveDto save(OperatorSaveDto operatorSaveDto);

    OperatorUpdateDto update(Integer id, OperatorUpdateDto operatorUpdateDto);

    T deleteById(Integer id);

    T get(Integer id);
}
