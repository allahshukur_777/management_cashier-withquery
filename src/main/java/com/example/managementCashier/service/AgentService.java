package com.example.managementCashier.service;

import com.example.managementCashier.dto.dto.AgentNameDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import java.util.List;

public interface AgentService<T>{

    List<T> getAll(Integer page, Integer size);

    AgentSaveDto save(AgentSaveDto agentSaveDto);

    AgentUpdateDto update(Integer id, AgentUpdateDto agentUpdateDto);

    T deleteById(Integer id);

    T get(Integer id);

    AgentNameDto getName(Integer id);
}
