package com.example.managementCashier.service.impl;

import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import com.example.managementCashier.exception.NotFoundException;
import com.example.managementCashier.repository.CashierRepository;
import com.example.managementCashier.service.CashierService;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CashierServiceImpl implements CashierService<CashierDto> {

    private final CashierRepository<CashierDto> cashierRepository;

    @Override
    public List<CashierDto> getAll(Integer page, Integer size) {
        if (page != null && size != null) {
            Pageable pageable = PageRequest.of(page, size);
            if (page > size) {
                throw new NotFoundException("Page cannot be bigger than size");
            } else if (cashierRepository.findAll(pageable).size() == 0) {
                throw new NotFoundException("There is not Cashier");
            } else {
                return cashierRepository.findAll(pageable);
            }
        } else {
            if (cashierRepository.findAll().size() == 0) {
                throw new NotFoundException("There is not Cashier");
            } else {
                return cashierRepository.findAll();
            }
        }
    }

    @Override
    public CashierSaveDto save(CashierSaveDto cashierSave) {
        if (!cashierRepository.findByZoneId(cashierSave.getZone())){
            throw new NotFoundException(cashierSave.getZone() + " Zone not found");
        } else if (!cashierRepository.findByCitiesId(cashierSave.getCities())){
            throw new NotFoundException(cashierSave.getCities() + " Cities not found");
        } else if (!cashierRepository.findByRegionId(cashierSave.getRegion())){
            throw new NotFoundException(cashierSave.getRegion() + " Region not found");
        } else if (!cashierRepository.findByStatusId(cashierSave.getStatus())){
            throw new NotFoundException(cashierSave.getStatus() + " Status not found");
        } else if (!cashierRepository.findByFreezeStatusId(cashierSave.getFreezeStatus())){
            throw new NotFoundException(cashierSave.getFreezeStatus() + " Freeze Status not found");
        } else if (!cashierRepository.findByAgentId(cashierSave.getAgentIdInCashier())){
            throw new NotFoundException(cashierSave.getAgentIdInCashier() + " Agent not found");
        } else {
            return cashierRepository.save(cashierSave);
        }
    }

    @Override
    public CashierUpdateDto update(Integer id, CashierUpdateDto cashierUpdate) {
        if (get(id) == null) {
            throw new NotFoundException(id +" Cashier not found");
        } else if (!cashierRepository.findByZoneId(cashierUpdate.getZone())){
            throw new NotFoundException(cashierUpdate.getZone() + " Zone not found");
        } else if (!cashierRepository.findByCitiesId(cashierUpdate.getCities())){
            throw new NotFoundException(cashierUpdate.getCities() + " Cities not found");
        } else if (!cashierRepository.findByRegionId(cashierUpdate.getRegion())){
            throw new NotFoundException(cashierUpdate.getRegion() + " Region not found");
        } else if (!cashierRepository.findByStatusId(cashierUpdate.getStatus())){
            throw new NotFoundException(cashierUpdate.getStatus() + " Status not found");
        } else if (!cashierRepository.findByFreezeStatusId(cashierUpdate.getFreezeStatus())){
            throw new NotFoundException(cashierUpdate.getFreezeStatus() + " Freeze Status not found");
        } else if (!cashierRepository.findByAgentId(cashierUpdate.getAgentIdInCashier())){
            throw new NotFoundException(cashierUpdate.getAgentIdInCashier() + " Agent not found");
        } else {
            return cashierRepository.update(id, cashierUpdate);
        }
    }

    @Override
    public CashierDto deleteById(Integer id) {
        if (cashierRepository.findById(id) == null) {
            throw new NotFoundException(id + " Cashier not found");
        } else {
            return cashierRepository.deleteById(id);
        }
    }

    @Override
    public CashierDto get(Integer id) {
        if (cashierRepository.findById(id) == null) {
            throw new NotFoundException(id + " Cashier not found");
        } else {
            return cashierRepository.findById(id);
        }
    }
}
