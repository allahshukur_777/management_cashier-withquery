package com.example.managementCashier.service.impl;

import  com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.dto.AgentNameDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import com.example.managementCashier.exception.NotFoundException;
import com.example.managementCashier.repository.AgentRepository;
import com.example.managementCashier.service.AgentService;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AgentServiceImpl implements AgentService<AgentDto> {

    private final AgentRepository<AgentDto> agentRepository;

    @Override
    public List<AgentDto> getAll(Integer page, Integer size) {
        if (page != null && size != null) {
            Pageable pageable = PageRequest.of(page, size);
            if (page > size) {
                throw new NotFoundException("Page cannot be larger than size");
            } else if (agentRepository.findAll(pageable).size() == 0) {
                throw new NotFoundException("There is not Agent");
            } else {
                return agentRepository.findAll(pageable);
            }
        } else {
            if (agentRepository.findAll().size() == 0) {
                throw new NotFoundException("There is not Agent");
            } else {
                return agentRepository.findAll();
            }
        }
    }

    @Override
    public AgentSaveDto save(AgentSaveDto agentSave) {
        if (!agentRepository.findByCitiesId(agentSave.getCities())) {
            throw new NotFoundException(agentSave.getCities() + " Cities not found");
        }  else if (!agentRepository.findByStatusId(agentSave.getStatus())) {
            throw new NotFoundException(agentSave.getStatus() + " Status not found");
        }  else if (!agentRepository.findByOperatorId(agentSave.getOperatorIdInAgent())) {
            throw new NotFoundException(agentSave.getOperatorIdInAgent() + " Operator not found");
        } else {
            return agentRepository.save(agentSave);
        }
    }

    @Override
    public AgentUpdateDto update(Integer id, AgentUpdateDto agentUpdate) {
        if (get(id) == null) {
            throw new NotFoundException(id + " Agent not found");
        } else if (!agentRepository.findByCitiesId(agentUpdate.getCities())) {
            throw new NotFoundException(agentUpdate.getCities() + " Cities not found");
        }  else if (!agentRepository.findByStatusId(agentUpdate.getStatus())) {
            throw new NotFoundException(agentUpdate.getStatus() + " Status not found");
        }  else if (!agentRepository.findByOperatorId(agentUpdate.getOperatorIdInAgent())) {
            throw new NotFoundException(agentUpdate.getOperatorIdInAgent() + " Operator not found");
        }  else {
            return agentRepository.update(id, agentUpdate);
        }
    }

    @Override
    public AgentDto deleteById(Integer id) {
        if (agentRepository.findById(id) == null) {
            throw new NotFoundException(id + " Agent not found");
        } else {
            return agentRepository.deleteById(id);
        }
    }

    @Override
    public AgentDto get(Integer id) {
        if (agentRepository.findById(id) == null) {
            throw new NotFoundException(id + " Agent not found");
        } else {
            return agentRepository.findById(id);
        }
    }

    @Override
    public AgentNameDto getName(Integer id) {
        AgentNameDto agentNameDto = agentRepository.getName(id);
        if (agentNameDto == null) {
            throw new NotFoundException(id + " Agent not found");
        }
        return agentNameDto;
    }
}
