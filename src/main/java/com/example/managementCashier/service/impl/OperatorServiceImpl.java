package com.example.managementCashier.service.impl;

import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import com.example.managementCashier.exception.ConnectException;
import com.example.managementCashier.exception.NotFoundException;
import com.example.managementCashier.repository.OperatorRepository;
import com.example.managementCashier.service.OperatorService;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OperatorServiceImpl implements OperatorService<OperatorDto> {

    private final OperatorRepository<OperatorDto> operatorRepository;

    @Override
    public List<OperatorDto> getAll(Integer page, Integer size) {
        if (page != null && size != null) {
            Pageable pageable = PageRequest.of(page, size);
            if (page > size) {
                throw new NotFoundException("Page cannot be larger than size");
            } else if (operatorRepository.findAll(pageable).size() == 0) {
                throw new NotFoundException("There is not Operator");
            } else {
                return operatorRepository.findAll(pageable);
            }
        } else {
            if (operatorRepository.findAll().size() == 0) {
                throw new NotFoundException("There is not Operator");
            } else {
                return operatorRepository.findAll();
            }
        }
    }

    @Override
    public OperatorSaveDto save(OperatorSaveDto operatorSave) {
        if (!operatorRepository.findByStatusId(operatorSave.getStatus())){
            throw new NotFoundException(operatorSave.getStatus() + " Status not found");
        } else {
            return operatorRepository.save(operatorSave);
        }
    }

    @Override
    public OperatorUpdateDto update(Integer id, OperatorUpdateDto operatorUpdate) {
        if (operatorUpdate.getOperatorId() == null) {
            throw new IllegalArgumentException(id + " Operator not found");
        } else if (!operatorRepository.findByStatusId(operatorUpdate.getStatus())){
            throw new NotFoundException(operatorUpdate.getStatus() + " Status not found");
        } else {
            return operatorRepository.update(id, operatorUpdate);
        }
    }

    @Override
    public OperatorDto deleteById(Integer id) {
        if (operatorRepository.findById(id) == null) {
            throw new ConnectException(id + " Operator not found");
        } else {
            return operatorRepository.deleteById(id);
        }
    }

    @Override
    public OperatorDto get(Integer id) {
        if (operatorRepository.findById(id) == null) {
            throw new NotFoundException(id + " Operator not found");
        } else {
            return operatorRepository.findById(id);
        }
    }
}
