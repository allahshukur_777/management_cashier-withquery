package com.example.managementCashier.service;

import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;

import java.util.List;

public interface CashierService<T>{

    List<T> getAll(Integer page, Integer size);

    CashierSaveDto save(CashierSaveDto cashierSaveDto);

    CashierUpdateDto update(Integer id, CashierUpdateDto cashierUpdateDto);

    T deleteById(Integer id);

    T get(Integer id);
}
