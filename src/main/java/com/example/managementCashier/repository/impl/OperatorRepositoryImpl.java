package com.example.managementCashier.repository.impl;

import com.example.managementCashier.connect.PsqlConnect;
import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.dto.StatusDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import com.example.managementCashier.repository.OperatorRepository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class OperatorRepositoryImpl implements OperatorRepository<OperatorDto> {
    private final PsqlConnect psqlConnect;
    private final ModelMapper modelMapper;

    @Override
    public List<OperatorDto> findAll() {
        List<OperatorDto> operatorList = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM operator " +
                    "LEFT JOIN agent a on operator.operator_id = a.operator_id_in_agent " +
                    "LEFT JOIN cashier c on a.agent_id = c.agent_id_in_cashier WHERE (operator.operator_delete = false AND a.agent_delete = false AND c.cashier_delete = false)\n" +
                    "OR (operator.operator_delete = false AND a.agent_delete = false) OR (operator.operator_delete = false)");
            ResultSet rs = preparedStatement.executeQuery();
            operatorList = returnResultSetAll(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return operatorList;
    }

    @Override
    public List<OperatorDto> findAll(Pageable pageable) {
        List<OperatorDto> operatorList = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM operator " +
                    "LEFT JOIN agent a on operator.operator_id = a.operator_id_in_agent " +
                    "LEFT JOIN cashier c on a.agent_id = c.agent_id_in_cashier WHERE (operator.operator_delete = false AND a.agent_delete = false AND c.cashier_delete = false)\n" +
                    "OR (operator.operator_delete = false AND a.agent_delete = false) OR (operator.operator_delete = false) " +
                    "OFFSET " + pageable.getPageNumber() + " LIMIT " + pageable.getPageSize());
            ResultSet rs = preparedStatement.executeQuery();
            operatorList = returnResultSetAll(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return operatorList;
    }

    @Override
    public OperatorSaveDto save(OperatorSaveDto operatorSave) {
        OperatorDto operator = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("" +
                    "INSERT INTO operator (name, headquarter_address, email_address, " +
                    "web_address, phone_number, fax_number, max_stake, min_stake, payout_limit, " +
                    "max_issue_amount_per_voucher, available_credit_limit, ticket_expiration_period, " +
                    "status, creation_date, type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            operator = modelMapper.map(operatorSave, OperatorDto.class);
            preparedStatement.setString(1, operator.getName());
            preparedStatement.setString(2, operator.getHeadquarterAddress());
            preparedStatement.setString(3, operator.getEmailAddress());
            preparedStatement.setString(4, operator.getWebAddress());
            preparedStatement.setString(5, operator.getPhoneNumber());
            preparedStatement.setString(6, operator.getFaxNumber());
            if (operator.getMaxStake() != null) {
                preparedStatement.setInt(7, operator.getMaxStake());
            } else {
                operator.setMaxStake(0);
                preparedStatement.setInt(7, operator.getMaxStake());
            }
            if (operator.getMinStake() != null) {
                preparedStatement.setInt(8, operator.getMinStake());
            } else {
                operator.setMinStake(0);
                preparedStatement.setInt(8, operator.getMinStake());
            }
            if (operator.getPayoutLimit() != null) {
                preparedStatement.setInt(9, operator.getPayoutLimit());
            } else {
                operator.setPayoutLimit(0);
                preparedStatement.setInt(9, operator.getPayoutLimit());
            }
            if (operator.getMaxIssueAmountPerVoucher() != null) {
                preparedStatement.setInt(10, operator.getMaxIssueAmountPerVoucher());
            } else {
                operator.setMaxIssueAmountPerVoucher(0);
                preparedStatement.setInt(10, operator.getMaxIssueAmountPerVoucher());
            }
            if (operator.getAvailableCreditLimit() != null) {
                preparedStatement.setInt(11, operator.getAvailableCreditLimit());
            } else {
                operator.setAvailableCreditLimit(0);
                preparedStatement.setInt(11, operator.getAvailableCreditLimit());
            }
            if (operator.getTicketExpirationPeriod() != null) {
                preparedStatement.setInt(12, operator.getTicketExpirationPeriod());
            } else {
                operator.setTicketExpirationPeriod(0);
                preparedStatement.setInt(12, operator.getTicketExpirationPeriod());
            }
            preparedStatement.setInt(13, operator.getStatus());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate creationLocalDate = LocalDate.parse(operator.getCreationDate(), formatter);
            preparedStatement.setDate(14, Date.valueOf(creationLocalDate));
            preparedStatement.setString(15, operator.getType());
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return modelMapper.map(operator, OperatorSaveDto.class);
    }

    @Override
    public OperatorUpdateDto update(Integer id, OperatorUpdateDto operatorUpdate) {
        OperatorDto operator = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("" +
                    "UPDATE operator SET name=?, headquarter_address=?, email_address=?, " +
                    "web_address=?, phone_number=?, fax_number=?, max_stake=?, min_stake=?, " +
                    "payout_limit=?, max_issue_amount_per_voucher=?, available_credit_limit=?, " +
                    "ticket_expiration_period=?, status=?, creation_date=?, type=? WHERE operator_id=?");
            operator = modelMapper.map(operatorUpdate, OperatorDto.class);
            preparedStatement.setString(1, operator.getName());
            preparedStatement.setString(2, operator.getHeadquarterAddress());
            preparedStatement.setString(3, operator.getEmailAddress());
            preparedStatement.setString(4, operator.getWebAddress());
            preparedStatement.setString(5, operator.getPhoneNumber());
            preparedStatement.setString(6, operator.getFaxNumber());
            if (operator.getMaxStake() != null) {
                preparedStatement.setInt(7, operator.getMaxStake());
            } else {
                operator.setMaxStake(0);
                preparedStatement.setInt(7, operator.getMaxStake());
            }
            if (operator.getMinStake() != null) {
                preparedStatement.setInt(8, operator.getMinStake());
            } else {
                operator.setMinStake(0);
                preparedStatement.setInt(8, operator.getMinStake());
            }
            if (operator.getPayoutLimit() != null) {
                preparedStatement.setInt(9, operator.getPayoutLimit());
            } else {
                operator.setPayoutLimit(0);
                preparedStatement.setInt(9, operator.getPayoutLimit());
            }
            if (operator.getMaxIssueAmountPerVoucher() != null) {
                preparedStatement.setInt(10, operator.getMaxIssueAmountPerVoucher());
            } else {
                operator.setMaxIssueAmountPerVoucher(0);
                preparedStatement.setInt(10, operator.getMaxIssueAmountPerVoucher());
            }
            if (operator.getAvailableCreditLimit() != null) {
                preparedStatement.setInt(11, operator.getAvailableCreditLimit());
            } else {
                operator.setAvailableCreditLimit(0);
                preparedStatement.setInt(11, operator.getAvailableCreditLimit());
            }
            if (operator.getTicketExpirationPeriod() != null) {
                preparedStatement.setInt(12, operator.getTicketExpirationPeriod());
            } else {
                operator.setTicketExpirationPeriod(0);
                preparedStatement.setInt(12, operator.getTicketExpirationPeriod());
            }
            preparedStatement.setInt(13, operator.getStatus());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate creationLocalDate = LocalDate.parse(operator.getCreationDate(), formatter);
            preparedStatement.setDate(14, Date.valueOf(creationLocalDate));
            preparedStatement.setString(15, operator.getType());
            operator.setOperatorId(id);
            preparedStatement.setInt(16, operator.getOperatorId());
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return modelMapper.map(operator, OperatorUpdateDto.class);
    }

    @Override
    public OperatorDto deleteById(Integer id) {
        OperatorDto operatorDto = findById(id);
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("WITH operator_agent AS (UPDATE operator SET operator_delete = true WHERE operator_id = ?),\n" +
                    "agent_cashier AS (UPDATE agent SET agent_delete = true WHERE operator_id_in_agent = ?)\n" +
                    "UPDATE cashier SET cashier_delete = true WHERE agent_id_in_cashier = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, id);
            preparedStatement.setInt(3, id);
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return operatorDto;
    }

    @Override
    public OperatorDto findById(Integer id) {
        OperatorDto operator = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM operator " +
                    "LEFT JOIN agent a on operator.operator_id = a.operator_id_in_agent " +
                    "LEFT JOIN cashier c on a.agent_id = c.agent_id_in_cashier WHERE (operator.operator_delete = false AND a.agent_delete = false AND c.cashier_delete = false)\n" +
                    "OR (operator.operator_delete = false AND a.agent_delete = false) OR (operator.operator_delete = false) " +
                    "AND operator_id=?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            operator = returnResultSetById(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return operator;
    }

    @Override
    public Boolean findByStatusId(Integer id) {
        boolean findStatus = false;
        StatusDto statusDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT status_id FROM status WHERE status_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            statusDto = returnResultSetStatusId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (statusDto != null) {
            findStatus = true;
        }
        return findStatus;
    }

    private List<OperatorDto> returnResultSetAll(ResultSet rs) throws Exception {
        Map<Integer, AgentDto> agentMap = new HashMap<>();
        Map<Integer, OperatorDto> operatorMap = new HashMap<>();
        while (rs.next()) {
            OperatorDto operator = operatorMap.get(rs.getInt("operator_id"));
            if (operator == null) {
                operator = new OperatorDto();
                operator.setOperatorId(rs.getInt("operator_id"));
                operator.setName(rs.getString("name"));
                operator.setHeadquarterAddress(rs.getString("headquarter_address"));
                operator.setEmailAddress(rs.getString("email_address"));
                operator.setWebAddress(rs.getString("web_address"));
                operator.setPhoneNumber(rs.getString("phone_number"));
                operator.setFaxNumber(rs.getString("fax_number"));
                operator.setMaxStake(rs.getInt("max_stake"));
                operator.setMinStake(rs.getInt("min_stake"));
                operator.setPayoutLimit(rs.getInt("payout_limit"));
                operator.setMaxIssueAmountPerVoucher(rs.getInt("max_issue_amount_per_voucher"));
                operator.setAvailableCreditLimit(rs.getInt("available_credit_limit"));
                operator.setTicketExpirationPeriod(rs.getInt("ticket_expiration_period"));
                operator.setStatus(rs.getInt("status"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                operator.setCreationDate(dateFormat.format(rs.getDate("creation_date")));
                operator.setType(rs.getString("type"));
                operatorMap.put(operator.getOperatorId(), operator);
            }

            AgentDto agent = agentMap.get(rs.getInt("agent_id"));
            if (agent == null) {
                if (rs.getInt("agent_id") != 0) {
                    agent = new AgentDto();
                    agent.setAgentId(rs.getInt("agent_id"));
                    agent.setAgentCode(rs.getInt("agent_code"));
                    agent.setFullName(rs.getString("full_name"));
                    agent.setIdNumber(rs.getInt("id_number"));
                    agent.setVoen(rs.getInt("voen"));
                    agent.setPhone(rs.getString("phone"));
                    agent.setMobile(rs.getString("mobile"));
                    agent.setEmail(rs.getString("email"));
                    agent.setSalesRepresentative(rs.getString("sales_representative"));
                    agent.setCities(rs.getInt("cities"));
                    agent.setAddress(rs.getString("address"));
                    agent.setTotalPermanentBalance(rs.getInt("total_permanent_balance"));
                    agent.setDebtCredit(rs.getInt("debt_credit"));
                    agent.setExtraDebtCredit(rs.getInt("extra_debt_credit"));
                    agent.setStatus(rs.getInt("agent_status"));
                    agent.setUserName(rs.getString("user_name"));
                    agent.setPassword(rs.getString("password"));
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    agent.setLastLogin(dateFormat.format(rs.getDate("last_login")));
                    agent.setCreationDate(dateFormat.format(rs.getDate("agent_creation_date")));
                    agent.setType(rs.getString("agent_type"));
                    agent.setOperatorIdInAgent(rs.getInt("operator_id_in_agent"));
                    agentMap.put(agent.getAgentId(), agent);
                    addAgentList(operator.getAgentList(), operator, agent);
                }
            }

            if (rs.getInt("cashier_id") != 0) {
                CashierDto cashier = new CashierDto();
                cashier.setCashierId(rs.getInt("cashier_id"));
                cashier.setCashierCode(rs.getInt("cashier_code"));
                cashier.setProviderId(rs.getString("provider_id"));
                cashier.setFullName(rs.getString("cashier_full_name"));
                cashier.setMobile(rs.getString("cashier_mobile"));
                cashier.setPhone(rs.getString("cashier_phone"));
                cashier.setEmail(rs.getString("cashier_email"));
                cashier.setSalesRepresentative(rs.getString("cashier_sales_representative"));
                cashier.setZone(rs.getInt("zone"));
                cashier.setCities(rs.getInt("cashier_cities"));
                cashier.setRegion(rs.getInt("region"));
                cashier.setAddress(rs.getString("cashier_address"));
                cashier.setMacAddress(rs.getString("cashier_mac_address"));
                cashier.setNextPermanentBalance(rs.getInt("next_permanent_balance"));
                cashier.setCurrentBalance(rs.getInt("current_balance"));
                cashier.setDebtCredit(rs.getInt("cashier_debt_credit"));
                cashier.setExtraDebtCredit(rs.getInt("cashier_extra_debt_credit"));
                cashier.setMinStake(rs.getInt("cashier_min_stake"));
                cashier.setMaxStake(rs.getInt("cashier_max_stake"));
                cashier.setBetTicketPayoutLimit(rs.getInt("bet_ticket_payout_limit"));
                cashier.setVoucherPayoutLimit(rs.getInt("voucher_payout_limit"));
                cashier.setStatus(rs.getInt("cashier_status"));
                cashier.setFreezeStatus(rs.getInt("freeze_status"));
                cashier.setUserName(rs.getString("cashier_user_name"));
                cashier.setPassword(rs.getString("cashier_password"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                cashier.setLastLogin(dateFormat.format(rs.getDate("cashier_last_login")));
                cashier.setLastLogout(dateFormat.format(rs.getDate("cashier_last_logout")));
                cashier.setCreationDate(dateFormat.format(rs.getDate("cashier_creation_date")));
                cashier.setAgentIdInCashier(rs.getInt("agent_id_in_cashier"));
                assert agent != null;
                addCashierList(agent.getCashierList(), agent, cashier);
            }
        }
        return new ArrayList<>(operatorMap.values());
    }

    private OperatorDto returnResultSetById(ResultSet rs) throws Exception {
        OperatorDto operator = null;
        Map<Integer, AgentDto> agentMap = new HashMap<>();
        Map<Integer, OperatorDto> operatorMap = new HashMap<>();
        while (rs.next()) {
            operator = operatorMap.get(rs.getInt("operator_id"));
            if (operator == null) {
                operator = new OperatorDto();
                operator.setOperatorId(rs.getInt("operator_id"));
                operator.setName(rs.getString("name"));
                operator.setHeadquarterAddress(rs.getString("headquarter_address"));
                operator.setEmailAddress(rs.getString("email_address"));
                operator.setWebAddress(rs.getString("web_address"));
                operator.setPhoneNumber(rs.getString("phone_number"));
                operator.setFaxNumber(rs.getString("fax_number"));
                operator.setMaxStake(rs.getInt("max_stake"));
                operator.setMinStake(rs.getInt("min_stake"));
                operator.setPayoutLimit(rs.getInt("payout_limit"));
                operator.setMaxIssueAmountPerVoucher(rs.getInt("max_issue_amount_per_voucher"));
                operator.setAvailableCreditLimit(rs.getInt("available_credit_limit"));
                operator.setTicketExpirationPeriod(rs.getInt("ticket_expiration_period"));
                operator.setStatus(rs.getInt("status"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                operator.setCreationDate(dateFormat.format(rs.getDate("creation_date")));
                operator.setType(rs.getString("type"));
                operatorMap.put(operator.getOperatorId(), operator);
            }

            AgentDto agent = agentMap.get(rs.getInt("agent_id"));
            if (agent == null) {
                if (rs.getInt("agent_id") != 0) {
                    agent = new AgentDto();
                    agent.setAgentId(rs.getInt("agent_id"));
                    agent.setAgentCode(rs.getInt("agent_code"));
                    agent.setFullName(rs.getString("full_name"));
                    agent.setIdNumber(rs.getInt("id_number"));
                    agent.setVoen(rs.getInt("voen"));
                    agent.setPhone(rs.getString("phone"));
                    agent.setMobile(rs.getString("mobile"));
                    agent.setEmail(rs.getString("email"));
                    agent.setSalesRepresentative(rs.getString("sales_representative"));
                    agent.setCities(rs.getInt("cities"));
                    agent.setAddress(rs.getString("address"));
                    agent.setTotalPermanentBalance(rs.getInt("total_permanent_balance"));
                    agent.setDebtCredit(rs.getInt("debt_credit"));
                    agent.setExtraDebtCredit(rs.getInt("extra_debt_credit"));
                    agent.setStatus(rs.getInt("agent_status"));
                    agent.setUserName(rs.getString("user_name"));
                    agent.setPassword(rs.getString("password"));
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    agent.setLastLogin(dateFormat.format(rs.getDate("last_login")));
                    agent.setCreationDate(dateFormat.format(rs.getDate("agent_creation_date")));
                    agent.setType(rs.getString("agent_type"));
                    agent.setOperatorIdInAgent(rs.getInt("operator_id_in_agent"));
                    agentMap.put(agent.getAgentId(), agent);
                    addAgentList(operator.getAgentList(), operator, agent);
                }
            }

            if (rs.getInt("cashier_id") != 0) {
                CashierDto cashier = new CashierDto();
                cashier.setCashierId(rs.getInt("cashier_id"));
                cashier.setCashierCode(rs.getInt("cashier_code"));
                cashier.setProviderId(rs.getString("provider_id"));
                cashier.setFullName(rs.getString("cashier_full_name"));
                cashier.setMobile(rs.getString("cashier_mobile"));
                cashier.setPhone(rs.getString("cashier_phone"));
                cashier.setEmail(rs.getString("cashier_email"));
                cashier.setSalesRepresentative(rs.getString("cashier_sales_representative"));
                cashier.setZone(rs.getInt("zone"));
                cashier.setCities(rs.getInt("cashier_cities"));
                cashier.setRegion(rs.getInt("region"));
                cashier.setAddress(rs.getString("cashier_address"));
                cashier.setMacAddress(rs.getString("cashier_mac_address"));
                cashier.setNextPermanentBalance(rs.getInt("next_permanent_balance"));
                cashier.setCurrentBalance(rs.getInt("current_balance"));
                cashier.setDebtCredit(rs.getInt("cashier_debt_credit"));
                cashier.setExtraDebtCredit(rs.getInt("cashier_extra_debt_credit"));
                cashier.setMinStake(rs.getInt("cashier_min_stake"));
                cashier.setMaxStake(rs.getInt("cashier_max_stake"));
                cashier.setBetTicketPayoutLimit(rs.getInt("bet_ticket_payout_limit"));
                cashier.setVoucherPayoutLimit(rs.getInt("voucher_payout_limit"));
                cashier.setStatus(rs.getInt("cashier_status"));
                cashier.setFreezeStatus(rs.getInt("freeze_status"));
                cashier.setUserName(rs.getString("cashier_user_name"));
                cashier.setPassword(rs.getString("cashier_password"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                cashier.setLastLogin(dateFormat.format(rs.getDate("cashier_last_login")));
                cashier.setLastLogout(dateFormat.format(rs.getDate("cashier_last_logout")));
                cashier.setCreationDate(dateFormat.format(rs.getDate("cashier_creation_date")));
                cashier.setAgentIdInCashier(rs.getInt("agent_id_in_cashier"));
                assert agent != null;
                addCashierList(agent.getCashierList(), agent, cashier);
            }
        }
        return operator;
    }

    private void addAgentList(List<AgentDto> agentList, OperatorDto operatorDto, AgentDto agentDto) {
        List<AgentDto> agentDtoList = agentList;
        if (agentDtoList == null) {
            agentDtoList = new ArrayList<>();
        }
        agentDtoList.add(agentDto);
        operatorDto.setAgentList(agentDtoList);
    }

    private void addCashierList(List<CashierDto> cashierList, AgentDto agentDto, CashierDto cashierDto) {
        List<CashierDto> cashierDtoList = cashierList;
        if (cashierDtoList == null) {
            cashierDtoList = new ArrayList<>();
        }
        cashierDtoList.add(cashierDto);
        agentDto.setCashierList(cashierDtoList);
    }

    private StatusDto returnResultSetStatusId(ResultSet rs) throws Exception {
        StatusDto statusDto = null;
        Map<Integer, StatusDto> statusMap = new HashMap<>();
        while (rs.next()) {
            statusDto = statusMap.get(rs.getInt("status_id"));
            if (statusDto == null) {
                statusDto = new StatusDto();
                statusDto.setStatusId(rs.getInt("status_id"));
                statusMap.put(statusDto.getStatusId(), statusDto);
            }
        }
        return statusDto;
    }
}
