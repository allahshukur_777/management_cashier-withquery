package com.example.managementCashier.repository.impl;

import com.example.managementCashier.connect.PsqlConnect;
import com.example.managementCashier.dto.dto.*;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import com.example.managementCashier.repository.CashierRepository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class CashierRepositoryImpl implements CashierRepository<CashierDto> {

    private final PsqlConnect psqlConnect;
    private final ModelMapper modelMapper;

    @Override
    public List<CashierDto> findAll() {
        List<CashierDto> cashierList = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM cashier WHERE cashier_delete = false");
            ResultSet rs = preparedStatement.executeQuery();
            cashierList = returnResultSetAll(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cashierList;
    }

    @Override
    public List<CashierDto> findAll(Pageable pageable) {
        List<CashierDto> cashierList = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM cashier WHERE cashier_delete = false OFFSET "
                    + pageable.getPageNumber() + " LIMIT " + pageable.getPageSize());
            ResultSet rs = preparedStatement.executeQuery();
            cashierList = returnResultSetAll(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cashierList;
    }

    @Override
    public CashierSaveDto save(CashierSaveDto cashierSave) {
        CashierDto cashier = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("" +
                    "INSERT INTO cashier(cashier_code, provider_id, cashier_full_name, cashier_mobile, " +
                    "cashier_phone, cashier_email, cashier_sales_representative, zone, cashier_cities, " +
                    "region, cashier_address, cashier_mac_address, next_permanent_balance, current_balance, " +
                    "cashier_debt_credit, cashier_extra_debt_credit, cashier_min_stake, cashier_max_stake, " +
                    "bet_ticket_payout_limit, voucher_payout_limit, cashier_status, freeze_status, " +
                    "cashier_user_name, cashier_password, cashier_last_login, cashier_last_logout," +
                    " cashier_creation_date, agent_id_in_cashier )" +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            cashier = modelMapper.map(cashierSave, CashierDto.class);
            preparedStatement.setInt(1, cashier.getCashierCode());
            preparedStatement.setString(2, cashier.getProviderId());
            preparedStatement.setString(3, cashier.getFullName());
            preparedStatement.setString(4, cashier.getMobile());
            preparedStatement.setString(5, cashier.getPhone());
            preparedStatement.setString(6, cashier.getEmail());
            preparedStatement.setString(7, cashier.getSalesRepresentative());
            preparedStatement.setInt(8, cashier.getZone());
            preparedStatement.setInt(9, cashier.getCities());
            preparedStatement.setInt(10, cashier.getRegion());
            preparedStatement.setString(11, cashier.getAddress());
            preparedStatement.setString(12, cashier.getMacAddress());
            if (cashier.getNextPermanentBalance() != null){
                preparedStatement.setInt(13, cashier.getNextPermanentBalance());
            } else {
                cashier.setNextPermanentBalance(0);
                preparedStatement.setInt(13, cashier.getNextPermanentBalance());
            }
            if (cashier.getCurrentBalance() != null){
                preparedStatement.setInt(14, cashier.getCurrentBalance());
            } else {
                cashier.setCurrentBalance(0);
                preparedStatement.setInt(14, cashier.getCurrentBalance());
            }
            if (cashier.getDebtCredit() != null){
                preparedStatement.setInt(15, cashier.getDebtCredit());
            } else {
                cashier.setDebtCredit(0);
                preparedStatement.setInt(15, cashier.getDebtCredit());
            }
            if (cashier.getExtraDebtCredit() != null){
                preparedStatement.setInt(16, cashier.getExtraDebtCredit());
            } else {
                cashier.setExtraDebtCredit(0);
                preparedStatement.setInt(16, cashier.getExtraDebtCredit());
            }
            if (cashier.getMinStake() != null){
                preparedStatement.setInt(17, cashier.getMinStake());
            } else {
                cashier.setMinStake(0);
                preparedStatement.setInt(17, cashier.getMinStake());
            }
            if (cashier.getMaxStake() != null){
                preparedStatement.setInt(18, cashier.getMaxStake());
            } else {
                cashier.setMaxStake(0);
                preparedStatement.setInt(18, cashier.getMaxStake());
            }
            if (cashier.getBetTicketPayoutLimit() != null){
                preparedStatement.setInt(19, cashier.getBetTicketPayoutLimit());
            } else {
                cashier.setBetTicketPayoutLimit(0);
                preparedStatement.setInt(19, cashier.getBetTicketPayoutLimit());
            }
            if (cashier.getVoucherPayoutLimit()!= null){
                preparedStatement.setInt(20, cashier.getVoucherPayoutLimit());
            } else {
                cashier.setVoucherPayoutLimit(0);
                preparedStatement.setInt(20, cashier.getVoucherPayoutLimit());
            }
            preparedStatement.setInt(21, cashier.getStatus());
            preparedStatement.setInt(22, cashier.getFreezeStatus());
            preparedStatement.setString(23, cashier.getUserName());
            preparedStatement.setString(24, cashier.getPassword());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate loginLocalDate = LocalDate.parse(cashier.getLastLogin(), formatter);
            LocalDate logoutLocalDate = LocalDate.parse(cashier.getLastLogout(), formatter);
            LocalDate creationLocalDate = LocalDate.parse(cashier.getCreationDate(), formatter);
            preparedStatement.setDate(25, Date.valueOf(loginLocalDate));
            preparedStatement.setDate(26, Date.valueOf(logoutLocalDate));
            preparedStatement.setDate(27, Date.valueOf(creationLocalDate));
            preparedStatement.setInt(28, cashier.getAgentIdInCashier());
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return modelMapper.map(cashier, CashierSaveDto.class);
    }

    @Override
    public CashierUpdateDto update(Integer id, CashierUpdateDto cashierUpdate) {
        CashierDto cashier = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("" +
                    " UPDATE cashier SET cashier_code=?, provider_id=?, cashier_full_name=?," +
                    " cashier_mobile=?, cashier_phone=?, cashier_email=?, cashier_sales_representative=?," +
                    " zone=?, cashier_cities=?, region=?, cashier_address=?, cashier_mac_address=?," +
                    " next_permanent_balance=?, current_balance=?, cashier_debt_credit=?," +
                    " cashier_extra_debt_credit=?, cashier_min_stake=?, cashier_max_stake=?," +
                    " bet_ticket_payout_limit=?, voucher_payout_limit=?, cashier_status=?," +
                    " freeze_status=?, cashier_user_name=?, cashier_password=?, cashier_last_login=?," +
                    " cashier_last_logout=?, cashier_creation_date=?, agent_id_in_cashier=? WHERE cashier_id=?");
            cashier = modelMapper.map(cashierUpdate, CashierDto.class);
            preparedStatement.setInt(1, cashier.getCashierCode());
            preparedStatement.setString(2, cashier.getProviderId());
            preparedStatement.setString(3, cashier.getFullName());
            preparedStatement.setString(4, cashier.getMobile());
            preparedStatement.setString(5, cashier.getPhone());
            preparedStatement.setString(6, cashier.getEmail());
            preparedStatement.setString(7, cashier.getSalesRepresentative());
            preparedStatement.setInt(8, cashier.getZone());
            preparedStatement.setInt(9, cashier.getCities());
            preparedStatement.setInt(10, cashier.getRegion());
            preparedStatement.setString(11, cashier.getAddress());
            preparedStatement.setString(12, cashier.getMacAddress());
            if (cashier.getNextPermanentBalance() != null){
                preparedStatement.setInt(13, cashier.getNextPermanentBalance());
            } else {
                cashier.setNextPermanentBalance(0);
                preparedStatement.setInt(13, cashier.getNextPermanentBalance());
            }
            if (cashier.getCurrentBalance() != null){
                preparedStatement.setInt(14, cashier.getCurrentBalance());
            } else {
                cashier.setCurrentBalance(0);
                preparedStatement.setInt(14, cashier.getCurrentBalance());
            }
            if (cashier.getDebtCredit() != null){
                preparedStatement.setInt(15, cashier.getDebtCredit());
            } else {
                cashier.setDebtCredit(0);
                preparedStatement.setInt(15, cashier.getDebtCredit());
            }
            if (cashier.getExtraDebtCredit() != null){
                preparedStatement.setInt(16, cashier.getExtraDebtCredit());
            } else {
                cashier.setExtraDebtCredit(0);
                preparedStatement.setInt(16, cashier.getExtraDebtCredit());
            }
            if (cashier.getMinStake() != null){
                preparedStatement.setInt(17, cashier.getMinStake());
            } else {
                cashier.setMinStake(0);
                preparedStatement.setInt(17, cashier.getMinStake());
            }
            if (cashier.getMaxStake() != null){
                preparedStatement.setInt(18, cashier.getMaxStake());
            } else {
                cashier.setMaxStake(0);
                preparedStatement.setInt(18, cashier.getMaxStake());
            }
            if (cashier.getBetTicketPayoutLimit() != null){
                preparedStatement.setInt(19, cashier.getBetTicketPayoutLimit());
            } else {
                cashier.setBetTicketPayoutLimit(0);
                preparedStatement.setInt(19, cashier.getBetTicketPayoutLimit());
            }
            if (cashier.getVoucherPayoutLimit()!= null){
                preparedStatement.setInt(20, cashier.getVoucherPayoutLimit());
            } else {
                cashier.setVoucherPayoutLimit(0);
                preparedStatement.setInt(20, cashier.getVoucherPayoutLimit());
            }
            preparedStatement.setInt(21, cashier.getStatus());
            preparedStatement.setInt(22, cashier.getFreezeStatus());
            preparedStatement.setString(23, cashier.getUserName());
            preparedStatement.setString(24, cashier.getPassword());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate loginLocalDate = LocalDate.parse(cashier.getLastLogin(), formatter);
            LocalDate logoutLocalDate = LocalDate.parse(cashier.getLastLogin(), formatter);
            LocalDate creationLocalDate = LocalDate.parse(cashier.getLastLogin(), formatter);
            preparedStatement.setDate(25, Date.valueOf(loginLocalDate));
            preparedStatement.setDate(26, Date.valueOf(logoutLocalDate));
            preparedStatement.setDate(27, Date.valueOf(creationLocalDate));
            preparedStatement.setInt(28, cashier.getAgentIdInCashier());
            cashier.setCashierId(id);
            preparedStatement.setInt(29, cashier.getCashierId());
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return modelMapper.map(cashier, CashierUpdateDto.class);
    }

    @Override
    public CashierDto deleteById(Integer id) {
        CashierDto cashierDto = findById(id);
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("UPDATE cashier SET cashier_delete = true WHERE cashier_id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cashierDto;
    }

    @Override
    public CashierDto findById(Integer id) {
        CashierDto cashier = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM cashier WHERE cashier_delete = false AND cashier_id=?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            cashier = returnResultSetById(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cashier;
    }

    @Override
    public Boolean findByZoneId(Integer id) {
        boolean findZone = false;
        ZoneDto zoneDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT zone_id FROM zone WHERE zone_id=?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            zoneDto = returnResultSetZoneId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (zoneDto != null) {
            findZone = true;
        }
        return findZone;
    }

    @Override
    public Boolean findByCitiesId(Integer id) {
        boolean findCities = false;
        CitiesDto citiesDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT cities_id FROM cities WHERE cities_id=?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            citiesDto = returnResultSetCitiesId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (citiesDto != null) {
            findCities = true;
        }
        return findCities;
    }

    @Override
    public Boolean findByRegionId(Integer id) {
        boolean findRegion = false;
        RegionDto regionDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT region_id FROM region WHERE region_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            regionDto = returnResultSetRegionId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (regionDto != null) {
            findRegion = true;
        }
        return findRegion;
    }

    @Override
    public Boolean findByStatusId(Integer id) {
        boolean findStatus = false;
        StatusDto statusDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT status_id FROM status WHERE status_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            statusDto = returnResultSetStatusId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (statusDto != null) {
            findStatus = true;
        }
        return findStatus;
    }

    @Override
    public Boolean findByFreezeStatusId(Integer id) {
        boolean findFreezeStatus = false;
        FreezeStatusDto freezeStatusDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT freeze_id FROM freeze_status WHERE freeze_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            freezeStatusDto = returnResultSetFreezeStatusId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (freezeStatusDto != null) {
            findFreezeStatus = true;
        }
        return findFreezeStatus;
    }

    @Override
    public Boolean findByAgentId(Integer id) {
        boolean findAgent = false;
        AgentDto agentDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT agent_id FROM agent WHERE agent_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            agentDto = returnResultSetAgentId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (agentDto != null) {
            findAgent = true;
        }
        return findAgent;
    }

    private List<CashierDto> returnResultSetAll(ResultSet rs) throws Exception {
        Map<Integer, CashierDto> cashierMap = new HashMap<>();
        while (rs.next()) {
            CashierDto cashier = cashierMap.get(rs.getInt("cashier_id"));
            if (cashier == null) {
                cashier = new CashierDto();
                cashier.setCashierId(rs.getInt("cashier_id"));
                cashier.setCashierCode(rs.getInt("cashier_code"));
                cashier.setProviderId(rs.getString("provider_id"));
                cashier.setFullName(rs.getString("cashier_full_name"));
                cashier.setMobile(rs.getString("cashier_mobile"));
                cashier.setPhone(rs.getString("cashier_phone"));
                cashier.setEmail(rs.getString("cashier_email"));
                cashier.setSalesRepresentative(rs.getString("cashier_sales_representative"));
                cashier.setZone(rs.getInt("zone"));
                cashier.setCities(rs.getInt("cashier_cities"));
                cashier.setRegion(rs.getInt("region"));
                cashier.setAddress(rs.getString("cashier_address"));
                cashier.setMacAddress(rs.getString("cashier_mac_address"));
                cashier.setNextPermanentBalance(rs.getInt("next_permanent_balance"));
                cashier.setCurrentBalance(rs.getInt("current_balance"));
                cashier.setDebtCredit(rs.getInt("cashier_debt_credit"));
                cashier.setExtraDebtCredit(rs.getInt("cashier_extra_debt_credit"));
                cashier.setMinStake(rs.getInt("cashier_min_stake"));
                cashier.setMaxStake(rs.getInt("cashier_max_stake"));
                cashier.setBetTicketPayoutLimit(rs.getInt("bet_ticket_payout_limit"));
                cashier.setVoucherPayoutLimit(rs.getInt("voucher_payout_limit"));
                cashier.setStatus(rs.getInt("cashier_status"));
                cashier.setFreezeStatus(rs.getInt("freeze_status"));
                cashier.setUserName(rs.getString("cashier_user_name"));
                cashier.setPassword(rs.getString("cashier_password"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                cashier.setLastLogin(dateFormat.format(rs.getDate("cashier_last_login")));
                cashier.setLastLogout(dateFormat.format(rs.getDate("cashier_last_logout")));
                cashier.setCreationDate(dateFormat.format(rs.getDate("cashier_creation_date")));
                cashier.setAgentIdInCashier(rs.getInt("agent_id_in_cashier"));
                cashierMap.put(cashier.getCashierId(), cashier);
            }
        }
        return new ArrayList<>(cashierMap.values());
    }

    private CashierDto returnResultSetById(ResultSet rs) throws Exception {
        CashierDto cashier = null;
        Map<Integer, CashierDto> cashierMap = new HashMap<>();
        while (rs.next()) {
            cashier = cashierMap.get(rs.getInt("cashier_id"));
            if (cashier == null) {
                cashier = new CashierDto();
                cashier.setCashierId(rs.getInt("cashier_id"));
                cashier.setCashierCode(rs.getInt("cashier_code"));
                cashier.setProviderId(rs.getString("provider_id"));
                cashier.setFullName(rs.getString("cashier_full_name"));
                cashier.setMobile(rs.getString("cashier_mobile"));
                cashier.setPhone(rs.getString("cashier_phone"));
                cashier.setEmail(rs.getString("cashier_email"));
                cashier.setSalesRepresentative(rs.getString("cashier_sales_representative"));
                cashier.setZone(rs.getInt("zone"));
                cashier.setCities(rs.getInt("cashier_cities"));
                cashier.setRegion(rs.getInt("region"));
                cashier.setAddress(rs.getString("cashier_address"));
                cashier.setMacAddress(rs.getString("cashier_mac_address"));
                cashier.setNextPermanentBalance(rs.getInt("next_permanent_balance"));
                cashier.setCurrentBalance(rs.getInt("current_balance"));
                cashier.setDebtCredit(rs.getInt("cashier_debt_credit"));
                cashier.setExtraDebtCredit(rs.getInt("cashier_extra_debt_credit"));
                cashier.setMinStake(rs.getInt("cashier_min_stake"));
                cashier.setMaxStake(rs.getInt("cashier_max_stake"));
                cashier.setBetTicketPayoutLimit(rs.getInt("bet_ticket_payout_limit"));
                cashier.setVoucherPayoutLimit(rs.getInt("voucher_payout_limit"));
                cashier.setStatus(rs.getInt("cashier_status"));
                cashier.setFreezeStatus(rs.getInt("freeze_status"));
                cashier.setUserName(rs.getString("cashier_user_name"));
                cashier.setPassword(rs.getString("cashier_password"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                cashier.setLastLogin(dateFormat.format(rs.getDate("cashier_last_login")));
                cashier.setLastLogout(dateFormat.format(rs.getDate("cashier_last_logout")));
                cashier.setCreationDate(dateFormat.format(rs.getDate("cashier_creation_date")));
                cashier.setAgentIdInCashier(rs.getInt("agent_id_in_cashier"));
                cashierMap.put(cashier.getCashierId(), cashier);
            }
        }
        return cashier;
    }

    private ZoneDto returnResultSetZoneId(ResultSet rs) throws Exception {
        ZoneDto zoneDto = null;
        Map<Integer, ZoneDto> zoneMap = new HashMap<>();
        while (rs.next()) {
            zoneDto = zoneMap.get(rs.getInt("zone_id"));
            if (zoneDto == null) {
                zoneDto = new ZoneDto();
                zoneDto.setZoneId(rs.getInt("zone_id"));
                zoneMap.put(zoneDto.getZoneId(), zoneDto);
            }
        }
        return zoneDto;
    }

    private CitiesDto returnResultSetCitiesId(ResultSet rs) throws Exception {
        CitiesDto citiesDto = null;
        Map<Integer, CitiesDto> citiesMap = new HashMap<>();
        while (rs.next()) {
            citiesDto = citiesMap.get(rs.getInt("cities_id"));
            if (citiesDto == null) {
                citiesDto = new CitiesDto();
                citiesDto.setCitiesId(rs.getInt("cities_id"));
                citiesMap.put(citiesDto.getCitiesId(), citiesDto);
            }
        }
        return citiesDto;
    }

    private RegionDto returnResultSetRegionId(ResultSet rs) throws Exception {
        RegionDto regionDto = null;
        Map<Integer, RegionDto> regionMap = new HashMap<>();
        while (rs.next()) {
            regionDto = regionMap.get(rs.getInt("region_id"));
            if (regionDto == null) {
                regionDto = new RegionDto();
                regionDto.setRegionId(rs.getInt("region_id"));
                regionMap.put(regionDto.getRegionId(), regionDto);
            }
        }
        return regionDto;
    }

    private StatusDto returnResultSetStatusId(ResultSet rs) throws Exception {
        StatusDto statusDto = null;
        Map<Integer, StatusDto> statusMap = new HashMap<>();
        while (rs.next()) {
            statusDto = statusMap.get(rs.getInt("status_id"));
            if (statusDto == null) {
                statusDto = new StatusDto();
                statusDto.setStatusId(rs.getInt("status_id"));
                statusMap.put(statusDto.getStatusId(), statusDto);
            }
        }
        return statusDto;
    }

    private FreezeStatusDto returnResultSetFreezeStatusId(ResultSet rs) throws Exception {
        FreezeStatusDto freezeStatusDto = null;
        Map<Integer, FreezeStatusDto> freezeStatusMap = new HashMap<>();
        while (rs.next()) {
            freezeStatusDto = freezeStatusMap.get(rs.getInt("freeze_id"));
            if (freezeStatusDto == null) {
                freezeStatusDto = new FreezeStatusDto();
                freezeStatusDto.setFreezeId(rs.getInt("freeze_id"));
                freezeStatusMap.put(freezeStatusDto.getFreezeId(), freezeStatusDto);
            }
        }
        return freezeStatusDto;
    }

    private AgentDto returnResultSetAgentId(ResultSet rs) throws Exception {
        AgentDto agentDto = null;
        Map<Integer, AgentDto> agentMap = new HashMap<>();
        while (rs.next()) {
            agentDto = agentMap.get(rs.getInt("agent_id"));
            if (agentDto == null) {
                agentDto = new AgentDto();
                agentDto.setAgentId(rs.getInt("agent_id"));
                agentMap.put(agentDto.getAgentId(), agentDto);
            }
        }
        return agentDto;
    }
}
