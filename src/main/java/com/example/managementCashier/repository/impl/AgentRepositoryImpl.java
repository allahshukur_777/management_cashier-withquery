package com.example.managementCashier.repository.impl;

import com.example.managementCashier.connect.PsqlConnect;
import com.example.managementCashier.dto.dto.*;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import com.example.managementCashier.repository.AgentRepository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class AgentRepositoryImpl implements AgentRepository<AgentDto> {

    private final PsqlConnect psqlConnect;
    private final ModelMapper modelMapper;

    @Override
    public List<AgentDto> findAll() {
        List<AgentDto> agentList = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM agent " +
                    "LEFT JOIN cashier c on agent.agent_id = c.agent_id_in_cashier WHERE (agent.agent_delete = false AND c.cashier_delete = false) " +
                    "OR (agent.agent_delete = false)");
            ResultSet rs = preparedStatement.executeQuery();
            agentList = returnResultSetAll(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return agentList;
    }

    @Override
    public List<AgentDto> findAll(Pageable pageable) {
        List<AgentDto> agentList = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM agent " +
                    "LEFT JOIN cashier c on agent.agent_id = c.agent_id_in_cashier WHERE (agent.agent_delete = false AND c.cashier_delete = false) " +
                    "OR (agent.agent_delete = false) OFFSET " + pageable.getPageNumber() + " LIMIT " + pageable.getPageSize());
            ResultSet rs = preparedStatement.executeQuery();
            agentList = returnResultSetAll(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return agentList;
    }

    @Override
    public AgentSaveDto save(AgentSaveDto agentSave) {
        AgentDto agent = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("" +
                    "INSERT INTO agent (agent_code, full_name, id_number, voen," +
                    " phone, mobile, email, sales_representative, cities, address, " +
                    "total_permanent_balance, debt_credit, extra_debt_credit, agent_status," +
                    " user_name, password, last_login, agent_creation_date, agent_type, operator_id_in_agent) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            agent = modelMapper.map(agentSave, AgentDto.class);
            preparedStatement.setInt(1, agent.getAgentCode());
            preparedStatement.setString(2, agent.getFullName());
            if (agent.getIdNumber() != null) {
                preparedStatement.setInt(3, agent.getIdNumber());
            } else {
                agent.setIdNumber(0);
                preparedStatement.setInt(3, agent.getIdNumber());
            }
            preparedStatement.setInt(4, agent.getVoen());
            preparedStatement.setString(5, agent.getPhone());
            preparedStatement.setString(6, agent.getMobile());
            preparedStatement.setString(7, agent.getEmail());
            preparedStatement.setString(8, agent.getSalesRepresentative());
            preparedStatement.setInt(9, agent.getCities());
            preparedStatement.setString(10, agent.getAddress());
            if (agent.getTotalPermanentBalance() != null){
                preparedStatement.setInt(11, agent.getTotalPermanentBalance());
            } else {
                agent.setTotalPermanentBalance(0);
                preparedStatement.setInt(11, agent.getTotalPermanentBalance());
            }
            if (agent.getDebtCredit() != null){
                preparedStatement.setInt(12, agent.getDebtCredit());
            } else {
                agent.setDebtCredit(0);
                preparedStatement.setInt(12, agent.getDebtCredit());
            }
            if (agent.getExtraDebtCredit() != null){
                preparedStatement.setInt(13, agent.getExtraDebtCredit());
            } else {
                agent.setExtraDebtCredit(0);
                preparedStatement.setInt(13, agent.getExtraDebtCredit());
            }
            preparedStatement.setInt(14, agent.getStatus());
            preparedStatement.setString(15, agent.getUserName());
            preparedStatement.setString(16, agent.getPassword());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate lastLocalDate = LocalDate.parse(agent.getLastLogin(), formatter);
            LocalDate creationLocalDate = LocalDate.parse(agent.getCreationDate(), formatter);
            preparedStatement.setDate(17, Date.valueOf(lastLocalDate));
            preparedStatement.setDate(18, Date.valueOf(creationLocalDate));
            preparedStatement.setString(19, agent.getType());
            preparedStatement.setInt(20, agent.getOperatorIdInAgent());
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return modelMapper.map(agent, AgentSaveDto.class);
    }

    @Override
    public AgentUpdateDto update(Integer id, AgentUpdateDto agentUpdate) {
        AgentDto agent = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("" +
                    " UPDATE agent SET agent_code=?, full_name=?, id_number=?," +
                    "voen=?, phone=?, mobile=?, email=?, sales_representative=?, cities=?," +
                    "address=?, total_permanent_balance=?, debt_credit=?, extra_debt_credit=?," +
                    "agent_status=?, user_name=?, password=?, last_login=?, agent_creation_date=?, " +
                    "agent_type=?, operator_id_in_agent=? WHERE agent_id=?");
            agent = modelMapper.map(agentUpdate, AgentDto.class);
            preparedStatement.setInt(1, agent.getAgentCode());
            preparedStatement.setString(2, agent.getFullName());
            if (agent.getIdNumber() != null) {
                preparedStatement.setInt(3, agent.getIdNumber());
            } else {
                agent.setIdNumber(0);
                preparedStatement.setInt(3, agent.getIdNumber());
            }
            preparedStatement.setInt(4, agent.getVoen());
            preparedStatement.setString(5, agent.getPhone());
            preparedStatement.setString(6, agent.getMobile());
            preparedStatement.setString(7, agent.getEmail());
            preparedStatement.setString(8, agent.getSalesRepresentative());
            preparedStatement.setInt(9, agent.getCities());
            preparedStatement.setString(10, agent.getAddress());
            if (agent.getTotalPermanentBalance() != null){
                preparedStatement.setInt(11, agent.getTotalPermanentBalance());
            } else {
                agent.setTotalPermanentBalance(0);
                preparedStatement.setInt(11, agent.getTotalPermanentBalance());
            }
            if (agent.getDebtCredit() != null){
                preparedStatement.setInt(12, agent.getDebtCredit());
            } else {
                agent.setDebtCredit(0);
                preparedStatement.setInt(12, agent.getDebtCredit());
            }
            if (agent.getExtraDebtCredit() != null){
                preparedStatement.setInt(13, agent.getExtraDebtCredit());
            } else {
                agent.setExtraDebtCredit(0);
                preparedStatement.setInt(13, agent.getExtraDebtCredit());
            }
            preparedStatement.setInt(14, agent.getStatus());
            preparedStatement.setString(15, agent.getUserName());
            preparedStatement.setString(16, agent.getPassword());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate lastLocalDate = LocalDate.parse(agent.getLastLogin(), formatter);
            LocalDate creationLocalDate = LocalDate.parse(agent.getCreationDate(), formatter);
            preparedStatement.setDate(17, Date.valueOf(lastLocalDate));
            preparedStatement.setDate(18, Date.valueOf(creationLocalDate));
            preparedStatement.setString(19, agent.getType());
            preparedStatement.setInt(20, agent.getOperatorIdInAgent());
            agent.setAgentId(id);
            preparedStatement.setInt(21, agent.getAgentId());
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return modelMapper.map(agent, AgentUpdateDto.class);
    }

    @Override
    public AgentDto deleteById(Integer id) {
        AgentDto agentDto = findById(id);
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("WITH agent_cashier AS (UPDATE agent SET agent_delete = true WHERE agent_id = ?) " +
                    "UPDATE cashier SET cashier_delete = true WHERE agent_id_in_cashier = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return agentDto;
    }

    @Override
    public AgentDto findById(Integer id) {
        AgentDto agent = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT * FROM agent " +
                    "LEFT JOIN cashier c on agent.agent_id = c.agent_id_in_cashier WHERE (agent.agent_delete = false AND c.cashier_delete = false) " +
                    "OR (agent.agent_delete = false) AND agent.agent_id=?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            agent = returnResultSetById(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return agent;
    }

    @Override
    public AgentNameDto getName(Integer id) {
        AgentNameDto agentNameDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT full_name FROM agent WHERE agent_delete = false AND agent_id=?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            agentNameDto = returnResultSetByName(rs, id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return agentNameDto;
    }

    @Override
    public Boolean findByCitiesId(Integer id) {
        boolean findCities = false;
        CitiesDto citiesDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT cities_id FROM cities WHERE cities_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            citiesDto = returnResultSetCitiesId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (citiesDto != null) {
            findCities = true;
        }
        return findCities;
    }

    @Override
    public Boolean findByStatusId(Integer id) {
        boolean findStatus = false;
        StatusDto statusDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT status_id FROM status WHERE status_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            statusDto = returnResultSetStatusId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (statusDto != null) {
            findStatus = true;
        }
        return findStatus;
    }

    @Override
    public Boolean findByOperatorId(Integer id) {
        boolean findOperator = false;
        OperatorDto operatorDto = null;
        try {
            PreparedStatement preparedStatement = psqlConnect.connection("SELECT operator_id FROM operator WHERE operator_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            operatorDto = returnResultSetOperatorId(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (operatorDto != null) {
            findOperator = true;
        }
        return findOperator;
    }

    private List<AgentDto> returnResultSetAll(ResultSet rs) throws Exception {
        Map<Integer, AgentDto> agentMap = new HashMap<>();
        while (rs.next()) {
            AgentDto agent = agentMap.get(rs.getInt("agent_id"));
            if (agent == null) {
                agent = new AgentDto();
                agent.setAgentId(rs.getInt("agent_id"));
                agent.setAgentCode(rs.getInt("agent_code"));
                agent.setFullName(rs.getString("full_name"));
                agent.setIdNumber(rs.getInt("id_number"));
                agent.setVoen(rs.getInt("voen"));
                agent.setPhone(rs.getString("phone"));
                agent.setMobile(rs.getString("mobile"));
                agent.setEmail(rs.getString("email"));
                agent.setSalesRepresentative(rs.getString("sales_representative"));
                agent.setCities(rs.getInt("cities"));
                agent.setAddress(rs.getString("address"));
                agent.setTotalPermanentBalance(rs.getInt("total_permanent_balance"));
                agent.setDebtCredit(rs.getInt("debt_credit"));
                agent.setExtraDebtCredit(rs.getInt("extra_debt_credit"));
                agent.setStatus(rs.getInt("agent_status"));
                agent.setUserName(rs.getString("user_name"));
                agent.setPassword(rs.getString("password"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                agent.setLastLogin(dateFormat.format(rs.getDate("last_login")));
                agent.setCreationDate(dateFormat.format(rs.getDate("agent_creation_date")));
                agent.setType(rs.getString("agent_type"));
                agent.setOperatorIdInAgent(rs.getInt("operator_id_in_agent"));
                agentMap.put(agent.getAgentId(), agent);
            }

            if (rs.getInt("cashier_id") != 0) {
                CashierDto cashier = new CashierDto();
                cashier.setCashierId(rs.getInt("cashier_id"));
                cashier.setCashierCode(rs.getInt("cashier_code"));
                cashier.setProviderId(rs.getString("provider_id"));
                cashier.setFullName(rs.getString("cashier_full_name"));
                cashier.setMobile(rs.getString("cashier_mobile"));
                cashier.setPhone(rs.getString("cashier_phone"));
                cashier.setEmail(rs.getString("cashier_email"));
                cashier.setSalesRepresentative(rs.getString("cashier_sales_representative"));
                cashier.setZone(rs.getInt("zone"));
                cashier.setCities(rs.getInt("cashier_cities"));
                cashier.setRegion(rs.getInt("region"));
                cashier.setAddress(rs.getString("cashier_address"));
                cashier.setMacAddress(rs.getString("cashier_mac_address"));
                cashier.setNextPermanentBalance(rs.getInt("next_permanent_balance"));
                cashier.setCurrentBalance(rs.getInt("current_balance"));
                cashier.setDebtCredit(rs.getInt("cashier_debt_credit"));
                cashier.setExtraDebtCredit(rs.getInt("cashier_extra_debt_credit"));
                cashier.setMinStake(rs.getInt("cashier_min_stake"));
                cashier.setMaxStake(rs.getInt("cashier_max_stake"));
                cashier.setBetTicketPayoutLimit(rs.getInt("bet_ticket_payout_limit"));
                cashier.setVoucherPayoutLimit(rs.getInt("voucher_payout_limit"));
                cashier.setStatus(rs.getInt("cashier_status"));
                cashier.setFreezeStatus(rs.getInt("freeze_status"));
                cashier.setUserName(rs.getString("cashier_user_name"));
                cashier.setPassword(rs.getString("cashier_password"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                cashier.setLastLogin(dateFormat.format(rs.getDate("cashier_last_login")));
                cashier.setLastLogout(dateFormat.format(rs.getDate("cashier_last_logout")));
                cashier.setCreationDate(dateFormat.format(rs.getDate("cashier_creation_date")));
                cashier.setAgentIdInCashier(rs.getInt("agent_id_in_cashier"));
                addCashierList(agent.getCashierList(), agent, cashier);
            }
        }
        return new ArrayList<>(agentMap.values());
    }

    private AgentDto returnResultSetById(ResultSet rs) throws Exception {
        AgentDto agent = null;
        Map<Integer, AgentDto> agentMap = new HashMap<>();
        while (rs.next()) {
            agent = agentMap.get(rs.getInt("agent_id"));
            if (agent == null) {
                agent = new AgentDto();
                agent.setAgentId(rs.getInt("agent_id"));
                agent.setAgentCode(rs.getInt("agent_code"));
                agent.setFullName(rs.getString("full_name"));
                agent.setIdNumber(rs.getInt("id_number"));
                agent.setVoen(rs.getInt("voen"));
                agent.setPhone(rs.getString("phone"));
                agent.setMobile(rs.getString("mobile"));
                agent.setEmail(rs.getString("email"));
                agent.setSalesRepresentative(rs.getString("sales_representative"));
                agent.setCities(rs.getInt("cities"));
                agent.setAddress(rs.getString("address"));
                agent.setTotalPermanentBalance(rs.getInt("total_permanent_balance"));
                agent.setDebtCredit(rs.getInt("debt_credit"));
                agent.setExtraDebtCredit(rs.getInt("extra_debt_credit"));
                agent.setStatus(rs.getInt("agent_status"));
                agent.setUserName(rs.getString("user_name"));
                agent.setPassword(rs.getString("password"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                agent.setLastLogin(dateFormat.format(rs.getDate("last_login")));
                agent.setCreationDate(dateFormat.format(rs.getDate("agent_creation_date")));
                agent.setType(rs.getString("agent_type"));
                agent.setOperatorIdInAgent(rs.getInt("operator_id_in_agent"));
                agentMap.put(agent.getAgentId(), agent);
            }

            if (rs.getInt("cashier_id") != 0) {
                CashierDto cashier = new CashierDto();
                cashier.setCashierId(rs.getInt("cashier_id"));
                cashier.setCashierCode(rs.getInt("cashier_code"));
                cashier.setProviderId(rs.getString("provider_id"));
                cashier.setFullName(rs.getString("cashier_full_name"));
                cashier.setMobile(rs.getString("cashier_mobile"));
                cashier.setPhone(rs.getString("cashier_phone"));
                cashier.setEmail(rs.getString("cashier_email"));
                cashier.setSalesRepresentative(rs.getString("cashier_sales_representative"));
                cashier.setZone(rs.getInt("zone"));
                cashier.setCities(rs.getInt("cashier_cities"));
                cashier.setRegion(rs.getInt("region"));
                cashier.setAddress(rs.getString("cashier_address"));
                cashier.setMacAddress(rs.getString("cashier_mac_address"));
                cashier.setNextPermanentBalance(rs.getInt("next_permanent_balance"));
                cashier.setCurrentBalance(rs.getInt("current_balance"));
                cashier.setDebtCredit(rs.getInt("cashier_debt_credit"));
                cashier.setExtraDebtCredit(rs.getInt("cashier_extra_debt_credit"));
                cashier.setMinStake(rs.getInt("cashier_min_stake"));
                cashier.setMaxStake(rs.getInt("cashier_max_stake"));
                cashier.setBetTicketPayoutLimit(rs.getInt("bet_ticket_payout_limit"));
                cashier.setVoucherPayoutLimit(rs.getInt("voucher_payout_limit"));
                cashier.setStatus(rs.getInt("cashier_status"));
                cashier.setFreezeStatus(rs.getInt("freeze_status"));
                cashier.setUserName(rs.getString("cashier_user_name"));
                cashier.setPassword(rs.getString("cashier_password"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                cashier.setLastLogin(dateFormat.format(rs.getDate("cashier_last_login")));
                cashier.setLastLogout(dateFormat.format(rs.getDate("cashier_last_logout")));
                cashier.setCreationDate(dateFormat.format(rs.getDate("cashier_creation_date")));
                cashier.setAgentIdInCashier(rs.getInt("agent_id_in_cashier"));
                addCashierList(agent.getCashierList(), agent, cashier);
            }
        }
        return agent;
    }

    private AgentNameDto returnResultSetByName(ResultSet rs, Integer id) throws Exception {
        AgentNameDto agent = null;
        Map<Integer, AgentNameDto> agentMap = new HashMap<>();
        while (rs.next()) {
            agent = agentMap.get(id);
            if (agent == null) {
                agent = new AgentNameDto();
                agent.setAgentName(rs.getString("full_name"));
                agentMap.put(id, agent);
            }
        }
        return agent;
    }

    private void addCashierList(List<CashierDto> cashierList, AgentDto agentDto, CashierDto cashierDto) {
        List<CashierDto> cashierDtoList = cashierList;
        if (cashierDtoList == null) {
            cashierDtoList = new ArrayList<>();
        }
        cashierDtoList.add(cashierDto);
        agentDto.setCashierList(cashierDtoList);
    }

    private CitiesDto returnResultSetCitiesId(ResultSet rs) throws Exception {
        CitiesDto citiesDto = null;
        Map<Integer, CitiesDto> citiesMap = new HashMap<>();
        while (rs.next()) {
            citiesDto = citiesMap.get(rs.getInt("cities_id"));
            if (citiesDto == null) {
                citiesDto = new CitiesDto();
                citiesDto.setCitiesId(rs.getInt("cities_id"));
                citiesMap.put(citiesDto.getCitiesId(), citiesDto);
            }
        }
        return citiesDto;
    }

    private StatusDto returnResultSetStatusId(ResultSet rs) throws Exception {
        StatusDto statusDto = null;
        Map<Integer, StatusDto> statusMap = new HashMap<>();
        while (rs.next()) {
            statusDto = statusMap.get(rs.getInt("status_id"));
            if (statusDto == null) {
                statusDto = new StatusDto();
                statusDto.setStatusId(rs.getInt("status_id"));
                statusMap.put(statusDto.getStatusId(), statusDto);
            }
        }
        return statusDto;
    }

    private OperatorDto returnResultSetOperatorId(ResultSet rs) throws Exception {
        OperatorDto operatorDto = null;
        Map<Integer, OperatorDto> operatorMap = new HashMap<>();
        while (rs.next()) {
            operatorDto = operatorMap.get(rs.getInt("operator_id"));
            if (operatorDto == null) {
                operatorDto = new OperatorDto();
                operatorDto.setOperatorId(rs.getInt("operator_id"));
                operatorMap.put(operatorDto.getOperatorId(), operatorDto);
            }
        }
        return operatorDto;
    }
}
