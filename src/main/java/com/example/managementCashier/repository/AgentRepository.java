package com.example.managementCashier.repository;

import com.example.managementCashier.dto.dto.AgentNameDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AgentRepository<T> {
    List<T> findAll();

    List<T> findAll(Pageable pageable);

    AgentSaveDto save(AgentSaveDto agentSaveDto);

    AgentUpdateDto update(Integer id, AgentUpdateDto agentUpdateDto);

    T deleteById(Integer id);

    T findById(Integer id);

    AgentNameDto getName(Integer id);

    Boolean findByCitiesId(Integer id);

    Boolean findByStatusId(Integer id);

    Boolean findByOperatorId(Integer id);
}
