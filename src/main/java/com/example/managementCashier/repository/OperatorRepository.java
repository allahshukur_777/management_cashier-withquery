package com.example.managementCashier.repository;

import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OperatorRepository<T> {

    List<T> findAll();

    List<T> findAll(Pageable pageable);

    OperatorSaveDto save(OperatorSaveDto operatorSaveDto);

    OperatorUpdateDto update(Integer id, OperatorUpdateDto operatorUpdateDto);

    T deleteById(Integer id);

    T findById(Integer id);

    Boolean findByStatusId(Integer id);
}
