package com.example.managementCashier.repository;

import com.example.managementCashier.dto.dto.ZoneDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CashierRepository<T> {

    List<T> findAll();

    List<T> findAll(Pageable pageable);

    CashierSaveDto save(CashierSaveDto cashierSaveDto);

    CashierUpdateDto update(Integer id, CashierUpdateDto cashierUpdateDto);

    T deleteById(Integer id);

    T findById(Integer id);

    Boolean findByZoneId(Integer id);

    Boolean findByCitiesId(Integer id);

    Boolean findByRegionId(Integer id);

    Boolean findByStatusId(Integer id);

    Boolean findByFreezeStatusId(Integer id);

    Boolean findByAgentId(Integer id);
}
