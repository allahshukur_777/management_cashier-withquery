package com.example.managementCashier.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class PsqlConnect {

    public PreparedStatement connection(String query) throws Exception {
        Connection con = connect();
        return con.prepareStatement(query);
    }
    private Connection connect() throws Exception{
            return DriverManager.getConnection(
                    "jdbc:postgresql://localhost:7765/management_db",
                    "sukur",
                    "sukur424");
    }
}
