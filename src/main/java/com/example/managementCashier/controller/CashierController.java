package com.example.managementCashier.controller;

import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import com.example.managementCashier.service.CashierService;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/cashier")
public class CashierController {
    private final CashierService<CashierDto> cashierService;

    @GetMapping
    public List<CashierDto> getAll(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        return cashierService.getAll(page, size);
    }

    @GetMapping("/{id}")
    public CashierDto get(@PathVariable Integer id) {
        return cashierService.get(id);
    }

    @PostMapping
    public CashierSaveDto save(@Valid @RequestBody CashierSaveDto cashierSave) {
        return cashierService.save(cashierSave);
    }

    @PutMapping("/{id}")
    public CashierUpdateDto update(@PathVariable Integer id, @Valid @RequestBody CashierUpdateDto cashierUpdate) {
        return cashierService.update(id, cashierUpdate);
    }

    @DeleteMapping("/{id}")
    public CashierDto deleteById(@PathVariable Integer id) {
        return cashierService.deleteById(id);
    }
}
