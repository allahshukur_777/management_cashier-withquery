package com.example.managementCashier.controller;

import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import com.example.managementCashier.exception.ErrorMessage;
import com.example.managementCashier.service.AgentService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/agent")
public class AgentController {

    public final AgentService<AgentDto> agentService;

    @GetMapping
    public List<AgentDto> getAll(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size){
        return agentService.getAll(page,size);
    }

    @GetMapping("/{id}")
    public AgentDto findById(@PathVariable Integer id){
        return agentService.get(id);
    }

    @PostMapping
    public AgentSaveDto save(@Valid @RequestBody AgentSaveDto agentSave){
        return agentService.save(agentSave);
    }

    @PutMapping("/{id}")
    public AgentUpdateDto update(@PathVariable Integer id, @Valid @RequestBody AgentUpdateDto agentUpdate){
        return agentService.update(id,agentUpdate);
    }

    @DeleteMapping("/{id}")
    public AgentDto delete(@PathVariable Integer id){
        return agentService.deleteById(id);
    }

    @GetMapping("/name/{id}")
    public Object getName(@PathVariable Integer id){
        return agentService.getName(id);
    }
}
