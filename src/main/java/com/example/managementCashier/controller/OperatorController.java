package com.example.managementCashier.controller;

import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import com.example.managementCashier.service.OperatorService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/operator")
public class OperatorController {
    private final OperatorService<OperatorDto> operatorService;

    @GetMapping
    public List<OperatorDto> getAll(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        return operatorService.getAll(page, size);
    }

    @GetMapping("/{id}")
    public OperatorDto get(@PathVariable Integer id) {
        return operatorService.get(id);
    }

    @PostMapping
    public OperatorSaveDto save(@Valid @RequestBody OperatorSaveDto operatorSave){
        return operatorService.save(operatorSave);
    }

    @PutMapping("/{id}")
    public OperatorUpdateDto update(@PathVariable Integer id,@Valid @RequestBody OperatorUpdateDto operatorUpdate) {
        return operatorService.update(id, operatorUpdate);
    }

    @DeleteMapping("/{id}")
    public OperatorDto deleteById(@PathVariable Integer id) {
        return operatorService.deleteById(id);
    }
}
