package com.example.managementCashier.dto.dto;

import java.sql.Date;
import lombok.Data;

@Data
public class CashierDto {
    private Integer cashierId;
    private Integer cashierCode;
    private String providerId;
    private String fullName;
    private String mobile;
    private String phone;
    private String email;
    private String salesRepresentative;
    private Integer zone;
    private Integer cities;
    private Integer region;
    private String address;
    private String macAddress;
    private Integer nextPermanentBalance;
    private Integer currentBalance;
    private Integer debtCredit;
    private Integer extraDebtCredit;
    private Integer minStake;
    private Integer maxStake;
    private Integer betTicketPayoutLimit;
    private Integer voucherPayoutLimit;
    private Integer status;
    private Integer freezeStatus;
    private String userName;
    private String password;
    private String lastLogin;
    private String lastLogout;
    private String creationDate;
    private Integer agentIdInCashier;
}
