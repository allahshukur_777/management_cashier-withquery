package com.example.managementCashier.dto.dto;

import java.util.List;
import lombok.Data;

@Data
public class OperatorDto {
    private Integer operatorId;
    private String name;
    private String type;
    private String headquarterAddress;
    private String emailAddress;
    private String webAddress;
    private String phoneNumber;
    private String faxNumber;
    private Integer maxStake;
    private Integer minStake;
    private Integer payoutLimit;
    private Integer maxIssueAmountPerVoucher;
    private Integer availableCreditLimit;
    private Integer ticketExpirationPeriod;
    private Integer status;
    private String creationDate;
    private List<AgentDto> agentList;
}
