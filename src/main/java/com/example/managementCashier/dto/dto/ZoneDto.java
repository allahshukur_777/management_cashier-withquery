package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class ZoneDto {
    private Integer zoneId;
    private String name;
}
