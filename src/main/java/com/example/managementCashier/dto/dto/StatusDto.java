package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class StatusDto {
    private Integer statusId;
    private String name;
}
