package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class FreezeStatusDto {
    private Integer freezeId;
    private String name;
}
