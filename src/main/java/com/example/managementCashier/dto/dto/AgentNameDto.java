package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class AgentNameDto {
    private String agentName;
}
