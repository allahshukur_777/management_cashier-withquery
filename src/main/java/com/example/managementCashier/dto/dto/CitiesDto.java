package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class CitiesDto {
    private Integer citiesId;
    private String name;
}
