package com.example.managementCashier.dto.dto;

import java.util.List;
import lombok.Data;

@Data
public class AgentDto {
    private Integer agentId;
    private Integer agentCode;
    private String fullName;
    private Integer idNumber;
    private Integer voen;
    private String phone;
    private String mobile;
    private String email;
    private String salesRepresentative;
    private Integer cities;
    private String address;
    private Integer totalPermanentBalance;
    private Integer debtCredit;
    private Integer extraDebtCredit;
    private Integer status;
    private String userName;
    private String password;
    private String lastLogin;
    private String creationDate;
    private String type;
    private Integer operatorIdInAgent;
    private List<CashierDto> cashierList;
}
