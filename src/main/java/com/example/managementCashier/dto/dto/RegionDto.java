package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class RegionDto {
    private Integer regionId;
    private String name;
}
