package com.example.managementCashier.dto.saveDto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class OperatorSaveDto {
    @NotBlank(message = "Name can not be null")
    private String name;
    @NotBlank(message = "Type can not be null")
    private String type;
    @NotNull(message = "Headquarter Address can not be null")
    private String headquarterAddress;
    @NotNull(message = "Email Address can not be null")
    @Email(message = "Email should be valid")
    private String emailAddress;
    @NotNull(message = "Web Address can not be null")
    private String webAddress;
    @NotNull(message = "Phone Number can not be null")
    @Pattern(regexp = "[(][^[+][0-9]]{4}[)][0-9]{2}[\\s][0-9]{3}[^[-][0-9]]{3}[^[-][0-9]]{3}", message = "Phone Number can be (+000)00 000-00-00")
    private String phoneNumber;
    @NotNull(message = "Fax Number can not be null")
    private String faxNumber;
    private Integer maxStake;
    private Integer minStake;
    private Integer payoutLimit;
    private Integer maxIssueAmountPerVoucher;
    private Integer availableCreditLimit;
    private Integer ticketExpirationPeriod;
    @NotNull(message = "Status can not be null")
    private Integer status;
    @NotNull(message = "Creation Date can not be null")
    @Pattern(regexp = "[0-9]{2}[^[-][0-9]]{3}[^[-][0-9]]{5}", message = "Creation Date can be 00-00-0000")
    private String creationDate;
}
