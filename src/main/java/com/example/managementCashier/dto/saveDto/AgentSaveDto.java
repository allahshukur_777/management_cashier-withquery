package com.example.managementCashier.dto.saveDto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class AgentSaveDto {
    @NotBlank(message = "Agent Code can not be null")
    @Size(min = 7,max = 7, message = "Agent Code can be 7 symbol")
    @Pattern(regexp = "[0-9]{7}", message = "Agent Code can not be symbol")
    private Integer agentCode;
    @NotBlank(message = "Full Name can not be null")
    private String fullName;
    private Integer idNumber;
    @NotBlank(message = "Voen can not be null")
    @Pattern(regexp = "[0-9]{7}")
    private Integer voen;
    @NotBlank(message = "Phone can not be null")
    @Pattern(regexp = "[(][^[+][0-9]]{4}[)][0-9]{2}[\\s][0-9]{3}[^[-][0-9]]{3}[^[-][0-9]]{3}", message = "Phone can be (+000)00 000-00-00")
    private String phone;
    @NotBlank(message = "Mobile can not be null")
    private String mobile;
    @NotBlank(message = "Email can not be null")
    @Email(message = "Email should be valid")
    private String email;
    private String salesRepresentative;
    @NotBlank(message = "Cities can not be null")
    private Integer cities;
    private String address;
    private Integer totalPermanentBalance;
    private Integer debtCredit;
    private Integer extraDebtCredit;
    @NotBlank(message = "Status can not be null")
    private Integer status;
    private String userName;
    private String password;
    @NotBlank(message = "Last Login can not be null")
    @Pattern(regexp = "[0-9]{2}[^[-][0-9]]{3}[^[-][0-9]]{5}", message = "Last Login can be 00-00-0000")
    private String lastLogin;
    @NotBlank(message = "Creation Date can not be null")
    @Pattern(regexp = "[0-9]{2}[^[-][0-9]]{3}[^[-][0-9]]{5}", message = "Creation Date can be 00-00-0000")
    private String creationDate;
    @NotBlank(message = "Type can not be null")
    private String type;
    @NotBlank(message = "Operator Id can not be null")
    private Integer operatorIdInAgent;
}
