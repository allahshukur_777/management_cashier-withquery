package com.example.managementCashier.dto.updateDto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class CashierUpdateDto {
    private Integer cashierId;
    @NotBlank(message = "Cashier Code can not be null")
    @Size(min = 6,max = 6, message = "Cashier Code can be 6 symbol")
    @Pattern(regexp = "[0-9]{6}", message = "Cashier Code can not be symbol")
    private Integer cashierCode;
    @NotBlank(message = "Provider Id can not be null")
    @Size(min = 10,max = 10, message = "Provider Id can be 10 symbol")
    private String providerId;
    @NotBlank(message = "Full Name can not be null")
    private String fullName;
    @NotBlank(message = "Mobile can not be null")
    private String mobile;
    private String phone;
    @NotBlank(message = "Email can not be null")
    @Email(message = "Email should be valid")
    private String email;
    private String salesRepresentative;
    @NotBlank(message = "Zone can not be null")
    private Integer zone;
    @NotBlank(message = "Cities can not be null")
    private Integer cities;
    @NotBlank(message = "Region can not be null")
    private Integer region;
    @NotBlank(message = "Address can not be null")
    private String address;
    private String macAddress;
    private Integer nextPermanentBalance;
    private Integer currentBalance;
    private Integer debtCredit;
    private Integer extraDebtCredit;
    private Integer minStake;
    private Integer maxStake;
    private Integer betTicketPayoutLimit;
    private Integer voucherPayoutLimit;
    @NotBlank(message = "Status can not be null")
    private Integer status;
    @NotBlank(message = "Freeze Status can not be null")
    private Integer freezeStatus;
    private String userName;
    private String password;
    @NotBlank(message = "Last Login can not be null")
    @Pattern(regexp = "[0-9]{2}[^[-][0-9]]{3}[^[-][0-9]]{5}", message = "Last Login can be 00-00-0000")
    private String lastLogin;
    @NotBlank(message = "Last Logout can not be null")
    @Pattern(regexp = "[0-9]{2}[^[-][0-9]]{3}[^[-][0-9]]{5}", message = "Last Logout can be 00-00-0000")
    private String lastLogout;
    @NotBlank(message = "Creation Date can not be null")
    @Pattern(regexp = "[0-9]{2}[^[-][0-9]]{3}[^[-][0-9]]{5}", message = "Creation Date can be 00-00-0000")
    private String creationDate;
    @NotBlank(message = "Agent Id can not be null")
    private Integer agentIdInCashier;
}
